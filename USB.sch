EESchema Schematic File Version 4
LIBS:SmartWatch Final-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_B_Micro J3
U 1 1 5D2BFF91
P 4400 3750
F 0 "J3" H 4457 4217 50  0000 C CNN
F 1 "USB_B_Micro" H 4457 4126 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex_47346-0001" H 4550 3700 50  0001 C CNN
F 3 "~" H 4550 3700 50  0001 C CNN
	1    4400 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0133
U 1 1 5D2C0AC9
P 4400 4250
F 0 "#PWR0133" H 4400 4000 50  0001 C CNN
F 1 "GND" H 4405 4077 50  0000 C CNN
F 2 "" H 4400 4250 50  0001 C CNN
F 3 "" H 4400 4250 50  0001 C CNN
	1    4400 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 4150 4400 4250
Wire Wire Line
	4300 4150 4300 4250
Wire Wire Line
	4300 4250 4400 4250
Connection ~ 4400 4250
$Comp
L USB_ESD:USBLC6-4SC6 IC2
U 1 1 5D3E1154
P 5500 3800
F 0 "IC2" H 6050 4065 50  0000 C CNN
F 1 "USBLC6-4SC6" H 6050 3974 50  0000 C CNN
F 2 "USB_ESD:SOT95P280X145-6N" H 6450 3900 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00047494.pdf" H 6450 3800 50  0001 L CNN
F 4 "TVS Diode Array Uni-Directional USBLC6-4SC6 17V, SOT-23 6-Pin" H 6450 3700 50  0001 L CNN "Description"
F 5 "1.45" H 6450 3600 50  0001 L CNN "Height"
F 6 "STMicroelectronics" H 6450 3500 50  0001 L CNN "Manufacturer_Name"
F 7 "USBLC6-4SC6" H 6450 3400 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "511-USBLC6-4SC6" H 6450 3300 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=511-USBLC6-4SC6" H 6450 3200 50  0001 L CNN "Mouser Price/Stock"
F 10 "6247700P" H 6450 3100 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/6247700P" H 6450 3000 50  0001 L CNN "RS Price/Stock"
F 12 "70389690" H 6450 2900 50  0001 L CNN "Allied_Number"
F 13 "http://www.alliedelec.com/stmicroelectronics-usblc6-4sc6/70389690/" H 6450 2800 50  0001 L CNN "Allied Price/Stock"
	1    5500 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3550 5250 3550
Wire Wire Line
	5250 3550 5250 3200
$Comp
L power:+5V #PWR0143
U 1 1 5D3E3C39
P 5250 3150
F 0 "#PWR0143" H 5250 3000 50  0001 C CNN
F 1 "+5V" H 5265 3323 50  0000 C CNN
F 2 "" H 5250 3150 50  0001 C CNN
F 3 "" H 5250 3150 50  0001 C CNN
	1    5250 3150
	1    0    0    -1  
$EndComp
$Comp
L ESD_DIODE:ESDA7P60-1U1M D1
U 1 1 5D3E4780
P 6950 3450
F 0 "D1" V 7204 3578 50  0000 L CNN
F 1 "ESDA7P60-1U1M" V 7295 3578 50  0000 L CNN
F 2 "ESD_DIODE:ESDA7P601U1M" H 7400 3450 50  0001 L CNN
F 3 "https://www.st.com/resource/en/datasheet/esda7p60-1u1m.pdf" H 7400 3350 50  0001 L CNN
F 4 "STMicroelectronics ESDA7P60-1U1M Uni-Directional TVS Diode, 700W peak, 2-Pin QFN1610" H 7400 3250 50  0001 L CNN "Description"
F 5 "0.6" H 7400 3150 50  0001 L CNN "Height"
F 6 "STMicroelectronics" H 7400 3050 50  0001 L CNN "Manufacturer_Name"
F 7 "ESDA7P60-1U1M" H 7400 2950 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "511-ESDA7P60-1U1M" H 7400 2850 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=511-ESDA7P60-1U1M" H 7400 2750 50  0001 L CNN "Mouser Price/Stock"
F 10 "1685850" H 7400 2650 50  0001 L CNN "RS Part Number"
F 11 "https://uk.rs-online.com/web/p/products/1685850" H 7400 2550 50  0001 L CNN "RS Price/Stock"
	1    6950 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	5250 3950 5250 3800
Wire Wire Line
	5250 3800 5500 3800
Wire Wire Line
	5500 3900 5300 3900
Wire Wire Line
	5300 3900 5300 4250
Wire Wire Line
	5300 4250 4400 4250
Wire Wire Line
	4700 3850 5000 3850
Wire Wire Line
	5200 3850 5200 4000
Wire Wire Line
	5200 4000 5500 4000
Wire Wire Line
	4700 3750 4850 3750
Wire Wire Line
	5550 3750 5550 3450
Wire Wire Line
	5550 3450 6650 3450
Wire Wire Line
	6650 3450 6650 4000
Wire Wire Line
	6650 4000 6600 4000
Wire Wire Line
	5250 3200 6950 3200
Wire Wire Line
	6950 3200 6950 3450
Connection ~ 5250 3200
Wire Wire Line
	5250 3200 5250 3150
$Comp
L power:GND #PWR0144
U 1 1 5D3E8A45
P 6950 4050
F 0 "#PWR0144" H 6950 3800 50  0001 C CNN
F 1 "GND" H 6955 3877 50  0000 C CNN
F 2 "" H 6950 4050 50  0001 C CNN
F 3 "" H 6950 4050 50  0001 C CNN
	1    6950 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 3750 4850 4600
Connection ~ 4850 3750
Wire Wire Line
	4850 3750 5550 3750
Wire Wire Line
	5000 3850 5000 4600
Connection ~ 5000 3850
Wire Wire Line
	5000 3850 5200 3850
Text GLabel 5000 4600 3    50   Input ~ 0
USB_D-
Text GLabel 4850 4600 3    50   Input ~ 0
USB_D+
Wire Wire Line
	4700 3950 5150 3950
Connection ~ 5150 3950
Wire Wire Line
	5150 3950 5250 3950
Text GLabel 5150 4600 3    50   Input ~ 0
USB_ID
Wire Wire Line
	5150 3950 5150 4600
Wire Notes Line
	3850 2850 7900 2850
Wire Notes Line
	7900 2850 7900 5100
Wire Notes Line
	7900 5100 3850 5100
Wire Notes Line
	3850 5100 3850 2850
Text Notes 5400 2800 0    50   ~ 0
USB with ESD protection
$EndSCHEMATC
