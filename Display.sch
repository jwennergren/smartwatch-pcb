EESchema Schematic File Version 4
LIBS:SmartWatch Final-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 504208-2410:504208-2410 J?
U 1 1 5D3678A6
P 5100 2950
F 0 "J?" H 5600 3215 50  0000 C CNN
F 1 "504208-2410" H 5600 3124 50  0000 C CNN
F 2 "504208-2410:5042082410" H 5950 3050 50  0001 L CNN
F 3 "https://www.mouser.com.tr/datasheet/2/276/5042082410_PCB_RECEPTACLES-938950.pdf" H 5950 2950 50  0001 L CNN
F 4 "Board to Board & Mezzanine Connectors SlimStack 0.4 B/B Rec 24Ckt" H 5950 2850 50  0001 L CNN "Description"
F 5 "0.87" H 5950 2750 50  0001 L CNN "Height"
F 6 "Molex" H 5950 2650 50  0001 L CNN "Manufacturer_Name"
F 7 "504208-2410" H 5950 2550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "538-504208-2410" H 5950 2450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=538-504208-2410" H 5950 2350 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 5950 2250 50  0001 L CNN "RS Part Number"
F 11 "" H 5950 2150 50  0001 L CNN "RS Price/Stock"
	1    5100 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D3678AC
P 4950 4450
F 0 "#PWR?" H 4950 4200 50  0001 C CNN
F 1 "GND" H 4955 4277 50  0000 C CNN
F 2 "" H 4950 4450 50  0001 C CNN
F 3 "" H 4950 4450 50  0001 C CNN
	1    4950 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 2950 4950 2950
Wire Wire Line
	4950 2950 4950 3250
Wire Wire Line
	5100 3050 4900 3050
Text GLabel 4900 3050 0    50   Input ~ 0
DSI_D0N
Wire Wire Line
	5100 3150 4900 3150
Text GLabel 4900 3150 0    50   Input ~ 0
DSI_D0P
Wire Wire Line
	5100 3250 4950 3250
Connection ~ 4950 3250
Wire Wire Line
	4950 3250 4950 4150
Wire Wire Line
	5100 3350 4900 3350
Text GLabel 4900 3350 0    50   Input ~ 0
DSI_CLKN
Wire Wire Line
	5100 3450 4900 3450
Text GLabel 4900 3450 0    50   Input ~ 0
DSI_CLKP
Wire Wire Line
	5100 4150 4950 4150
Connection ~ 4950 4150
Wire Wire Line
	4950 4150 4950 4250
Wire Wire Line
	5100 4250 4950 4250
Connection ~ 4950 4250
Wire Wire Line
	4950 4250 4950 4450
$EndSCHEMATC
