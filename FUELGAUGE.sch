EESchema Schematic File Version 4
LIBS:SmartWatch Final-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Fuel_gauge:MAX17262REWL+T IC2
U 1 1 5D3D1257
P 5150 3500
F 0 "IC2" H 5700 3765 50  0000 C CNN
F 1 "MAX17262REWL+T" H 5700 3674 50  0000 C CNN
F 2 "BGA9C40P3X3_147X145X69" H 6100 3600 50  0001 L CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX17262.pdf" H 6100 3500 50  0001 L CNN
F 4 "Battery Management 5.2 A 1-Cell Fuel Gauge with ModelGauge m5 EZ and Internal CurrentSensing" H 6100 3400 50  0001 L CNN "Description"
F 5 "0.69" H 6100 3300 50  0001 L CNN "Height"
F 6 "Maxim Integrated" H 6100 3200 50  0001 L CNN "Manufacturer_Name"
F 7 "MAX17262REWL+T" H 6100 3100 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "700-MAX17262REWL+T" H 6100 3000 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=700-MAX17262REWL%2BT" H 6100 2900 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6100 2800 50  0001 L CNN "RS Part Number"
F 11 "" H 6100 2700 50  0001 L CNN "RS Price/Stock"
	1    5150 3500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
