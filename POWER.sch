EESchema Schematic File Version 4
LIBS:SmartWatch Final-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 4uf7:GRM035R60J475ME15D C9
U 1 1 5D3DA0B9
P 3900 3500
F 0 "C9" V 4104 3628 50  0000 L CNN
F 1 "GRM035R60J475ME15D" V 4195 3628 50  0000 L CNN
F 2 "4uf7:CAPC0603X55N" H 4250 3550 50  0001 L CNN
F 3 "https://psearch.en.murata.com/capacitor/product/GRM035R60J475ME15%23.pdf" H 4250 3450 50  0001 L CNN
F 4 "Multilayer Ceramic Capacitors MLCC - SMD/SMT 0201 4.7uF 6.3volts *Derate Voltage/Temp" H 4250 3350 50  0001 L CNN "Description"
F 5 "0.55" H 4250 3250 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 4250 3150 50  0001 L CNN "Manufacturer_Name"
F 7 "GRM035R60J475ME15D" H 4250 3050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-GRM035R60J475ME5D" H 4250 2950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-GRM035R60J475ME5D" H 4250 2850 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 4250 2750 50  0001 L CNN "RS Part Number"
F 11 "" H 4250 2650 50  0001 L CNN "RS Price/Stock"
	1    3900 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 3500 4950 3500
$Comp
L power:+BATT #PWR0105
U 1 1 5D3DD295
P 3900 3500
F 0 "#PWR0105" H 3900 3350 50  0001 C CNN
F 1 "+BATT" H 3915 3673 50  0000 C CNN
F 2 "" H 3900 3500 50  0001 C CNN
F 3 "" H 3900 3500 50  0001 C CNN
	1    3900 3500
	1    0    0    -1  
$EndComp
Connection ~ 3900 3500
$Comp
L power:GND #PWR0128
U 1 1 5D3DD76C
P 3900 4000
F 0 "#PWR0128" H 3900 3750 50  0001 C CNN
F 1 "GND" H 3905 3827 50  0000 C CNN
F 2 "" H 3900 4000 50  0001 C CNN
F 3 "" H 3900 4000 50  0001 C CNN
	1    3900 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0129
U 1 1 5D3DDB8B
P 5400 4050
F 0 "#PWR0129" H 5400 3800 50  0001 C CNN
F 1 "GND" H 5405 3877 50  0000 C CNN
F 2 "" H 5400 4050 50  0001 C CNN
F 3 "" H 5400 4050 50  0001 C CNN
	1    5400 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 3700 4950 3700
Wire Wire Line
	4950 3700 4950 3500
Connection ~ 4950 3500
Wire Wire Line
	4950 3500 3900 3500
$Comp
L L:CDRH4D28NP-100NC L3
U 1 1 5D3DDEF2
P 5700 3500
F 0 "L3" H 6100 3725 50  0000 C CNN
F 1 "CDRH4D28NP-100NC" H 6100 3634 50  0000 C CNN
F 2 "L:CDRH4D28NP100NC" H 6350 3550 50  0001 L CNN
F 3 "https://products.sumida.com/products/pdf/CDRH4D28.pdf" H 6350 3450 50  0001 L CNN
F 4 "Fixed Inductors 10uH 1.00A" H 6350 3350 50  0001 L CNN "Description"
F 5 "" H 6350 3250 50  0001 L CNN "Height"
F 6 "Sumida" H 6350 3150 50  0001 L CNN "Manufacturer_Name"
F 7 "CDRH4D28NP-100NC" H 6350 3050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "851-CDRH4D28NP-100NC" H 6350 2950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=851-CDRH4D28NP-100NC" H 6350 2850 50  0001 L CNN "Mouser Price/Stock"
F 10 "6637734" H 6350 2750 50  0001 L CNN "RS Part Number"
F 11 "https://uk.rs-online.com/web/p/products/6637734" H 6350 2650 50  0001 L CNN "RS Price/Stock"
	1    5700 3500
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:TPS62203DBV U6
U 1 1 5D3833F0
P 5400 3700
F 0 "U6" H 5400 4125 50  0000 C CNN
F 1 "TPS62203DBV" H 5400 4034 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5450 3550 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/tps62201.pdf" H 5400 3800 50  0001 C CNN
	1    5400 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3700 6500 3700
Wire Wire Line
	6500 3700 6500 3500
$Comp
L 10uF:GRM188R61A106KE69J C10
U 1 1 5D3E0C97
P 6650 3500
F 0 "C10" V 6854 3628 50  0000 L CNN
F 1 "GRM188R61A106KE69J" V 6945 3628 50  0000 L CNN
F 2 "10uf:CAPC1608X90N" H 7000 3550 50  0001 L CNN
F 3 "http://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.pdf" H 7000 3450 50  0001 L CNN
F 4 "Multilayer Ceramic Capacitors MLCC - SMD/SMT 0603 10uF 10volts *Derate Voltage/Temp" H 7000 3350 50  0001 L CNN "Description"
F 5 "0.9" H 7000 3250 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 7000 3150 50  0001 L CNN "Manufacturer_Name"
F 7 "GRM188R61A106KE69J" H 7000 3050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-GRM188R61A106KE9J" H 7000 2950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-GRM188R61A106KE9J" H 7000 2850 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 7000 2750 50  0001 L CNN "RS Part Number"
F 11 "" H 7000 2650 50  0001 L CNN "RS Price/Stock"
	1    6650 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 3500 6650 3500
Connection ~ 6500 3500
Wire Wire Line
	6650 3500 7100 3500
Connection ~ 6650 3500
Wire Wire Line
	5400 3900 5400 4000
Wire Wire Line
	6650 4000 5400 4000
Connection ~ 5400 4000
Wire Wire Line
	5400 4000 5400 4050
$Comp
L power:VDD #PWR0130
U 1 1 5D3E4410
P 7100 3500
F 0 "#PWR0130" H 7100 3350 50  0001 C CNN
F 1 "VDD" H 7117 3673 50  0000 C CNN
F 2 "" H 7100 3500 50  0001 C CNN
F 3 "" H 7100 3500 50  0001 C CNN
	1    7100 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5D3E584B
P 3900 5150
F 0 "TP1" H 3958 5268 50  0000 L CNN
F 1 "GND" H 3958 5177 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 4100 5150 50  0001 C CNN
F 3 "~" H 4100 5150 50  0001 C CNN
	1    3900 5150
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5D3E5E32
P 4600 5150
F 0 "TP2" H 4658 5268 50  0000 L CNN
F 1 "Batt+" H 4658 5177 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 4800 5150 50  0001 C CNN
F 3 "~" H 4800 5150 50  0001 C CNN
	1    4600 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 5150 4600 5300
$Comp
L power:+BATT #PWR0131
U 1 1 5D3E611F
P 4600 5300
F 0 "#PWR0131" H 4600 5150 50  0001 C CNN
F 1 "+BATT" H 4615 5473 50  0000 C CNN
F 2 "" H 4600 5300 50  0001 C CNN
F 3 "" H 4600 5300 50  0001 C CNN
	1    4600 5300
	-1   0    0    1   
$EndComp
Wire Wire Line
	3900 5150 3900 5300
$Comp
L power:GND #PWR0132
U 1 1 5D3E6D31
P 3900 5300
F 0 "#PWR0132" H 3900 5050 50  0001 C CNN
F 1 "GND" H 3905 5127 50  0000 C CNN
F 2 "" H 3900 5300 50  0001 C CNN
F 3 "" H 3900 5300 50  0001 C CNN
	1    3900 5300
	1    0    0    -1  
$EndComp
Wire Notes Line
	7750 5650 7750 3050
Wire Notes Line
	7750 3050 3500 3050
Wire Notes Line
	3500 3050 3500 5650
Wire Notes Line
	3500 5650 7750 5650
Text Notes 5450 3000 0    50   ~ 0
SMPS
$EndSCHEMATC
