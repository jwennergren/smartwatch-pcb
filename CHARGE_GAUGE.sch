EESchema Schematic File Version 4
LIBS:SmartWatch Final-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CHARGE_GAUGE:STC3117IJT IC1
U 1 1 5D3CD973
P 5250 3700
F 0 "IC1" H 5800 3965 50  0000 C CNN
F 1 "STC3117IJT" H 5800 3874 50  0000 C CNN
F 2 "FUEL_CHARGE:BGA9C40P3X3_146X156X66" H 6200 3800 50  0001 L CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/ea/b5/01/6e/dd/f0/49/3e/DM00105047.pdf/files/DM00105047.pdf/jcr:content/translations/en.DM00105047.pdf" H 6200 3700 50  0001 L CNN
F 4 "Battery Management ANALOG" H 6200 3600 50  0001 L CNN "Description"
F 5 "0.655" H 6200 3500 50  0001 L CNN "Height"
F 6 "STMicroelectronics" H 6200 3400 50  0001 L CNN "Manufacturer_Name"
F 7 "STC3117IJT" H 6200 3300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "511-STC3117IJT" H 6200 3200 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=511-STC3117IJT" H 6200 3100 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6200 3000 50  0001 L CNN "RS Part Number"
F 11 "" H 6200 2900 50  0001 L CNN "RS Price/Stock"
	1    5250 3700
	1    0    0    -1  
$EndComp
Text GLabel 5250 3800 0    50   Input ~ 0
I2C3_SDA
Text GLabel 5250 4100 0    50   Input ~ 0
I2C3_SCL
$EndSCHEMATC
