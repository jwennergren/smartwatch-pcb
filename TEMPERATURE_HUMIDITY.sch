EESchema Schematic File Version 4
LIBS:SmartWatch Final-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Temp_humidity:HTU21D HM1
U 1 1 5D3D2C9C
P 5450 3900
F 0 "HM1" H 6050 4165 50  0000 C CNN
F 1 "HTU21D" H 6050 4074 50  0000 C CNN
F 2 "Temp_meter:SON100P300X300X100-7N-D" H 6500 4000 50  0001 L CNN
F 3 "http://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7FHPC199_6%7FA6%7Fpdf%7FEnglish%7FENG_DS_HPC199_6_A6.pdf%7FCAT-HSC0004" H 6500 3900 50  0001 L CNN
F 4 "SENSOR SOLUTIONS - TE CONNECTIVITY - HTU21D - HUMIDITY/TEMP SENSOR, DIGITAL, DFN-6" H 6500 3800 50  0001 L CNN "Description"
F 5 "1" H 6500 3700 50  0001 L CNN "Height"
F 6 "TE Connectivity" H 6500 3600 50  0001 L CNN "Manufacturer_Name"
F 7 "HTU21D" H 6500 3500 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "824-HTU21D" H 6500 3400 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=824-HTU21D" H 6500 3300 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6500 3200 50  0001 L CNN "RS Part Number"
F 11 "" H 6500 3100 50  0001 L CNN "RS Price/Stock"
	1    5450 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 4000 7000 4000
Wire Wire Line
	7000 4000 7000 3750
$Comp
L power:VDD #PWR0134
U 1 1 5D3D3E5C
P 7000 3750
F 0 "#PWR0134" H 7000 3600 50  0001 C CNN
F 1 "VDD" H 7017 3923 50  0000 C CNN
F 2 "" H 7000 3750 50  0001 C CNN
F 3 "" H 7000 3750 50  0001 C CNN
	1    7000 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 4000 5300 4000
Wire Wire Line
	5300 4000 5300 4450
$Comp
L power:GND #PWR0138
U 1 1 5D3D40D3
P 5300 4550
F 0 "#PWR0138" H 5300 4300 50  0001 C CNN
F 1 "GND" H 5305 4377 50  0000 C CNN
F 2 "" H 5300 4550 50  0001 C CNN
F 3 "" H 5300 4550 50  0001 C CNN
	1    5300 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 4600 5650 4600
Wire Wire Line
	5650 4600 5650 4450
Wire Wire Line
	5650 4450 5300 4450
Connection ~ 5300 4450
Wire Wire Line
	5300 4450 5300 4550
Wire Wire Line
	5450 3900 5200 3900
Wire Wire Line
	6650 3900 6650 3700
Text GLabel 5200 3900 0    50   Input ~ 0
I2C3_SDA
Text GLabel 6650 3700 1    50   Input ~ 0
I2C3_SCL
Wire Notes Line
	4500 3200 7300 3200
Wire Notes Line
	7300 3200 7300 4900
Wire Notes Line
	7300 4900 4500 4900
Wire Notes Line
	4500 4900 4500 3200
Text Notes 5300 3150 0    50   ~ 0
Temperature & humidity sensor
$EndSCHEMATC
