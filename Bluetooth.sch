EESchema Schematic File Version 4
LIBS:SmartWatch Final-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BLUETOOTH_MODULES:EYSHJNZWZ U?
U 1 1 5D3B4224
P 4500 3000
AR Path="/5D3B4224" Ref="U?"  Part="1" 
AR Path="/5D3B3262/5D3B4224" Ref="U4"  Part="1" 
F 0 "U4" H 5650 3265 50  0000 C CNN
F 1 "EYSHJNZWZ" H 5650 3174 50  0000 C CNN
F 2 "BLUETOOTH_MODULES:EYSHJNZWZ_1" H 6650 3100 50  0001 L CNN
F 3 "http://www.yuden.co.jp/wireless_module/document/datareport2/jp/TY_BLE_EYSHJNZWZ_DataReport_V1_0_20170630J.pdf" H 6650 3000 50  0001 L CNN
F 4 "Bluetooth  low energy Module" H 6650 2900 50  0001 L CNN "Description"
F 5 "" H 6650 2800 50  0001 L CNN "Height"
F 6 "TAIYO YUDEN" H 6650 2700 50  0001 L CNN "Manufacturer_Name"
F 7 "EYSHJNZWZ" H 6650 2600 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "963-EYSHJNZWZ" H 6650 2500 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=963-EYSHJNZWZ" H 6650 2400 50  0001 L CNN "Mouser Price/Stock"
F 10 "1806558" H 6650 2300 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/1806558" H 6650 2200 50  0001 L CNN "RS Price/Stock"
	1    4500 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3000 3450 3800
$Comp
L power:GND #PWR0139
U 1 1 5D3CF5A6
P 3450 4550
F 0 "#PWR0139" H 3450 4300 50  0001 C CNN
F 1 "GND" H 3455 4377 50  0000 C CNN
F 2 "" H 3450 4550 50  0001 C CNN
F 3 "" H 3450 4550 50  0001 C CNN
	1    3450 4550
	1    0    0    -1  
$EndComp
Connection ~ 3450 3800
Wire Wire Line
	3450 3800 3450 4100
Connection ~ 3450 4100
Wire Wire Line
	3450 4100 3450 4400
Wire Wire Line
	6800 3000 7100 3000
Wire Wire Line
	7100 3000 7100 3500
$Comp
L power:GND #PWR0140
U 1 1 5D3CFADE
P 7100 4550
F 0 "#PWR0140" H 7100 4300 50  0001 C CNN
F 1 "GND" H 7105 4377 50  0000 C CNN
F 2 "" H 7100 4550 50  0001 C CNN
F 3 "" H 7100 4550 50  0001 C CNN
	1    7100 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 3500 7100 3500
Connection ~ 7100 3500
Wire Wire Line
	7100 3500 7100 4300
Wire Wire Line
	6800 4300 7100 4300
Connection ~ 7100 4300
Wire Wire Line
	7100 4300 7100 4550
Wire Wire Line
	3250 3400 3250 3000
$Comp
L power:VDD #PWR0141
U 1 1 5D3D0593
P 3250 2750
F 0 "#PWR0141" H 3250 2600 50  0001 C CNN
F 1 "VDD" H 3267 2923 50  0000 C CNN
F 2 "" H 3250 2750 50  0001 C CNN
F 3 "" H 3250 2750 50  0001 C CNN
	1    3250 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4200 4500 4300
$Comp
L 4uf7:GRM035R60J475ME15D C13
U 1 1 5D3D178D
P 2750 3000
F 0 "C13" H 3000 3265 50  0000 C CNN
F 1 "GRM035R60J475ME15D" H 3000 3174 50  0000 C CNN
F 2 "4uf7:CAPC0603X55N" H 3100 3050 50  0001 L CNN
F 3 "https://psearch.en.murata.com/capacitor/product/GRM035R60J475ME15%23.pdf" H 3100 2950 50  0001 L CNN
F 4 "Multilayer Ceramic Capacitors MLCC - SMD/SMT 0201 4.7uF 6.3volts *Derate Voltage/Temp" H 3100 2850 50  0001 L CNN "Description"
F 5 "0.55" H 3100 2750 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 3100 2650 50  0001 L CNN "Manufacturer_Name"
F 7 "GRM035R60J475ME15D" H 3100 2550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-GRM035R60J475ME5D" H 3100 2450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-GRM035R60J475ME5D" H 3100 2350 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 3100 2250 50  0001 L CNN "RS Part Number"
F 11 "" H 3100 2150 50  0001 L CNN "RS Price/Stock"
	1    2750 3000
	1    0    0    -1  
$EndComp
Connection ~ 3250 3000
Wire Wire Line
	3250 3000 3250 2750
$Comp
L 100nF:GRM022R61A104ME01L C16
U 1 1 5D3D312A
P 2750 3400
F 0 "C16" H 3000 3665 50  0000 C CNN
F 1 "GRM022R61A104ME01L" H 3000 3574 50  0000 C CNN
F 2 "100nF:CAPC0402X22N" H 3100 3450 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/GRM022R61A104ME01L.pdf" H 3100 3350 50  0001 L CNN
F 4 "Multilayer Ceramic Capacitors MLCC - SMD/SMT" H 3100 3250 50  0001 L CNN "Description"
F 5 "0.22" H 3100 3150 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 3100 3050 50  0001 L CNN "Manufacturer_Name"
F 7 "GRM022R61A104ME01L" H 3100 2950 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-GRM022R61A104ME1L" H 3100 2850 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-GRM022R61A104ME1L" H 3100 2750 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 3100 2650 50  0001 L CNN "RS Part Number"
F 11 "" H 3100 2550 50  0001 L CNN "RS Price/Stock"
	1    2750 3400
	1    0    0    -1  
$EndComp
Connection ~ 3250 3400
Wire Wire Line
	2750 3000 2750 3400
Wire Wire Line
	2750 3400 2750 4400
Wire Wire Line
	2750 4400 3450 4400
Connection ~ 2750 3400
Connection ~ 3450 4400
Wire Wire Line
	3450 4400 3450 4550
$Comp
L Device:Crystal_Small Y?
U 1 1 5D3D57C5
P 3900 3150
AR Path="/5D3D57C5" Ref="Y?"  Part="1" 
AR Path="/5D3B4C3F/5D3D57C5" Ref="Y?"  Part="1" 
AR Path="/5D3B3262/5D3D57C5" Ref="Y2"  Part="1" 
F 0 "Y2" V 3946 3238 50  0000 L CNN
F 1 "Crystal_Small" V 3855 3238 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_2012-2Pin_2.0x1.2mm" H 3900 3150 50  0001 C CNN
F 3 "~" H 3900 3150 50  0001 C CNN
	1    3900 3150
	0    1    -1   0   
$EndComp
Wire Wire Line
	4500 3050 4500 3100
Wire Wire Line
	4500 3250 4500 3200
Wire Wire Line
	3450 3000 4500 3000
Wire Wire Line
	3250 3400 4500 3400
Wire Wire Line
	3450 3800 4500 3800
Wire Wire Line
	3450 4100 4500 4100
Wire Wire Line
	3900 3050 3950 3050
Wire Wire Line
	3900 3250 4150 3250
Wire Wire Line
	6800 3100 7250 3100
Wire Wire Line
	6800 3200 7250 3200
Wire Wire Line
	6800 3300 7250 3300
$Comp
L Connector:TestPoint TP?
U 1 1 5D3FF7DD
P 8800 5200
AR Path="/5D3FF7DD" Ref="TP?"  Part="1" 
AR Path="/5D3B3262/5D3FF7DD" Ref="TP11"  Part="1" 
F 0 "TP11" H 8858 5318 50  0000 L CNN
F 1 "TestPoint" H 8858 5227 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 9000 5200 50  0001 C CNN
F 3 "~" H 9000 5200 50  0001 C CNN
	1    8800 5200
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 5D3FF7E3
P 9250 5200
AR Path="/5D3FF7E3" Ref="TP?"  Part="1" 
AR Path="/5D3B3262/5D3FF7E3" Ref="TP13"  Part="1" 
F 0 "TP13" H 9308 5318 50  0000 L CNN
F 1 "TestPoint" H 9308 5227 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 9450 5200 50  0001 C CNN
F 3 "~" H 9450 5200 50  0001 C CNN
	1    9250 5200
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 5D3FF7EF
P 8800 5600
AR Path="/5D3FF7EF" Ref="TP?"  Part="1" 
AR Path="/5D3B3262/5D3FF7EF" Ref="TP12"  Part="1" 
F 0 "TP12" H 8858 5718 50  0000 L CNN
F 1 "TestPoint" H 8858 5627 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 9000 5600 50  0001 C CNN
F 3 "~" H 9000 5600 50  0001 C CNN
	1    8800 5600
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 5D3FF7F5
P 9250 5600
AR Path="/5D3FF7F5" Ref="TP?"  Part="1" 
AR Path="/5D3B3262/5D3FF7F5" Ref="TP14"  Part="1" 
F 0 "TP14" H 9308 5718 50  0000 L CNN
F 1 "TestPoint" H 9308 5627 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 9450 5600 50  0001 C CNN
F 3 "~" H 9450 5600 50  0001 C CNN
	1    9250 5600
	1    0    0    -1  
$EndComp
Text GLabel 7250 3100 2    50   Input ~ 0
BLUETOOTH_SWDIO
Text GLabel 7250 3200 2    50   Input ~ 0
BLUETOOTH_SWDCLK
Wire Wire Line
	8800 5200 8650 5200
Text GLabel 8650 5200 0    50   Input ~ 0
BLUETOOTH_SWDIO
Wire Wire Line
	9250 5200 9700 5200
Text GLabel 9700 5200 2    50   Input ~ 0
BLUETOOTH_SWDCLK
$Comp
L power:VDD #PWR0155
U 1 1 5D408879
P 9250 5600
F 0 "#PWR0155" H 9250 5450 50  0001 C CNN
F 1 "VDD" H 9268 5773 50  0000 C CNN
F 2 "" H 9250 5600 50  0001 C CNN
F 3 "" H 9250 5600 50  0001 C CNN
	1    9250 5600
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0156
U 1 1 5D409572
P 8800 5600
F 0 "#PWR0156" H 8800 5350 50  0001 C CNN
F 1 "GND" H 8805 5427 50  0000 C CNN
F 2 "" H 8800 5600 50  0001 C CNN
F 3 "" H 8800 5600 50  0001 C CNN
	1    8800 5600
	1    0    0    -1  
$EndComp
Text GLabel 8700 3400 2    50   Input ~ 0
BLUETOOTH_USART_RX
Text GLabel 8700 3600 2    50   Input ~ 0
BLUETOOTH_USART_TX
Wire Wire Line
	6800 3400 8350 3400
Wire Wire Line
	6800 3600 8200 3600
$Comp
L Connector:TestPoint TP?
U 1 1 5D419D9D
P 8200 3600
AR Path="/5D419D9D" Ref="TP?"  Part="1" 
AR Path="/5D3B3262/5D419D9D" Ref="TP9"  Part="1" 
F 0 "TP9" H 8258 3718 50  0000 L CNN
F 1 "TestPoint" H 8258 3627 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 8400 3600 50  0001 C CNN
F 3 "~" H 8400 3600 50  0001 C CNN
	1    8200 3600
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 5D41AA75
P 8350 3400
AR Path="/5D41AA75" Ref="TP?"  Part="1" 
AR Path="/5D3B3262/5D41AA75" Ref="TP10"  Part="1" 
F 0 "TP10" H 8408 3518 50  0000 L CNN
F 1 "TestPoint" H 8408 3427 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 8550 3400 50  0001 C CNN
F 3 "~" H 8550 3400 50  0001 C CNN
	1    8350 3400
	1    0    0    -1  
$EndComp
Connection ~ 8200 3600
Wire Wire Line
	8200 3600 8700 3600
Connection ~ 8350 3400
Wire Wire Line
	8350 3400 8700 3400
$Comp
L 0402_cap:CBR04C279B5GAC C?
U 1 1 5D425C41
P 4150 3250
F 0 "C?" V 4446 3122 50  0000 R CNN
F 1 "CBR04C279B5GAC" V 4355 3122 50  0000 R CNN
F 2 "CAPC1005X55N" H 4500 3300 50  0001 L CNN
F 3 "http://www.kemet.com/Lists/ProductCatalog/Attachments/490/KEM_C1030_CBR_SMD.pdf" H 4500 3200 50  0001 L CNN
F 4 "Kemet 0402 Capactior Chip" H 4500 3100 50  0001 L CNN "Description"
F 5 "0.55" H 4500 3000 50  0001 L CNN "Height"
F 6 "Kemet" H 4500 2900 50  0001 L CNN "Manufacturer_Name"
F 7 "CBR04C279B5GAC" H 4500 2800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "80-CBR04C279B5G" H 4500 2700 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=80-CBR04C279B5G" H 4500 2600 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 4500 2500 50  0001 L CNN "RS Part Number"
F 11 "" H 4500 2400 50  0001 L CNN "RS Price/Stock"
	1    4150 3250
	0    -1   -1   0   
$EndComp
Connection ~ 4150 3250
Wire Wire Line
	4150 3250 4500 3250
$Comp
L 0402_cap:CBR04C279B5GAC C?
U 1 1 5D42741B
P 3950 2550
F 0 "C?" V 4154 2678 50  0000 L CNN
F 1 "CBR04C279B5GAC" V 4245 2678 50  0000 L CNN
F 2 "CAPC1005X55N" H 4300 2600 50  0001 L CNN
F 3 "http://www.kemet.com/Lists/ProductCatalog/Attachments/490/KEM_C1030_CBR_SMD.pdf" H 4300 2500 50  0001 L CNN
F 4 "Kemet 0402 Capactior Chip" H 4300 2400 50  0001 L CNN "Description"
F 5 "0.55" H 4300 2300 50  0001 L CNN "Height"
F 6 "Kemet" H 4300 2200 50  0001 L CNN "Manufacturer_Name"
F 7 "CBR04C279B5GAC" H 4300 2100 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "80-CBR04C279B5G" H 4300 2000 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=80-CBR04C279B5G" H 4300 1900 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 4300 1800 50  0001 L CNN "RS Part Number"
F 11 "" H 4300 1700 50  0001 L CNN "RS Price/Stock"
	1    3950 2550
	0    1    1    0   
$EndComp
Connection ~ 3950 3050
Wire Wire Line
	3950 3050 4500 3050
$Comp
L power:GND #PWR?
U 1 1 5D428651
P 3950 2550
F 0 "#PWR?" H 3950 2300 50  0001 C CNN
F 1 "GND" H 3955 2377 50  0000 C CNN
F 2 "" H 3950 2550 50  0001 C CNN
F 3 "" H 3950 2550 50  0001 C CNN
	1    3950 2550
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D428CF4
P 4150 2750
F 0 "#PWR?" H 4150 2500 50  0001 C CNN
F 1 "GND" H 4155 2577 50  0000 C CNN
F 2 "" H 4150 2750 50  0001 C CNN
F 3 "" H 4150 2750 50  0001 C CNN
	1    4150 2750
	-1   0    0    1   
$EndComp
$EndSCHEMATC
