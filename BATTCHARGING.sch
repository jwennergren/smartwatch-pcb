EESchema Schematic File Version 4
LIBS:SmartWatch Final-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LIPO_charging:BQ25125YFPT IC1
U 1 1 5D3CEB92
P 5000 3150
F 0 "IC1" H 5750 3415 50  0000 C CNN
F 1 "BQ25125YFPT" H 5750 3324 50  0000 C CNN
F 2 "BGA25C40P5X5_247X253X50" H 6350 3250 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/bq25125.pdf" H 6350 3150 50  0001 L CNN
F 4 "Battery Management Low IQ highly integrated battery charge management solution for wearables and IoT" H 6350 3050 50  0001 L CNN "Description"
F 5 "0.5" H 6350 2950 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 6350 2850 50  0001 L CNN "Manufacturer_Name"
F 7 "BQ25125YFPT" H 6350 2750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "595-BQ25125YFPT" H 6350 2650 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=595-BQ25125YFPT" H 6350 2550 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6350 2450 50  0001 L CNN "RS Part Number"
F 11 "" H 6350 2350 50  0001 L CNN "RS Price/Stock"
	1    5000 3150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
