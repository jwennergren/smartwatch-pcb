EESchema Schematic File Version 4
LIBS:SmartWatch Final-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FH19C-8S-05SH:FH19C-8S-0.5SH(10) J?
U 1 1 5D3AB1C3
P 8800 1200
AR Path="/5D3AB1C3" Ref="J?"  Part="1" 
AR Path="/5D3A7172/5D3AB1C3" Ref="J2"  Part="1" 
F 0 "J2" H 9250 1465 50  0000 C CNN
F 1 "FH19C-8S-0.5SH(10)" H 9250 1374 50  0000 C CNN
F 2 "FH19C-8S-05SH:FH19C8S05SH10" H 9550 1300 50  0001 L CNN
F 3 "http://www.mouser.com/datasheet/2/185/FH19SC_catalog-939358.pdf" H 9550 1200 50  0001 L CNN
F 4 "HIROSE(HRS) - FH19C-8S-0.5SH(10) - CONN, FPC, RCPT, R/A, 8POS, 0.5MM, SMT" H 9550 1100 50  0001 L CNN "Description"
F 5 "1.6" H 9550 1000 50  0001 L CNN "Height"
F 6 "Hirose" H 9550 900 50  0001 L CNN "Manufacturer_Name"
F 7 "FH19C-8S-0.5SH(10)" H 9550 800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "798-FH19C8S05SH10" H 9550 700 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=798-FH19C8S05SH10" H 9550 600 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 9550 500 50  0001 L CNN "RS Part Number"
F 11 "" H 9550 400 50  0001 L CNN "RS Price/Stock"
	1    8800 1200
	1    0    0    -1  
$EndComp
$Comp
L 504208-2410:504208-2410 J?
U 1 1 5D3AB1D1
P 2000 1400
AR Path="/5D3AB1D1" Ref="J?"  Part="1" 
AR Path="/5D3A7172/5D3AB1D1" Ref="J1"  Part="1" 
F 0 "J1" H 2500 1665 50  0000 C CNN
F 1 "504208-2410" H 2500 1574 50  0000 C CNN
F 2 "504208-2410:5042082410" H 2850 1500 50  0001 L CNN
F 3 "https://www.mouser.com.tr/datasheet/2/276/5042082410_PCB_RECEPTACLES-938950.pdf" H 2850 1400 50  0001 L CNN
F 4 "Board to Board & Mezzanine Connectors SlimStack 0.4 B/B Rec 24Ckt" H 2850 1300 50  0001 L CNN "Description"
F 5 "0.87" H 2850 1200 50  0001 L CNN "Height"
F 6 "Molex" H 2850 1100 50  0001 L CNN "Manufacturer_Name"
F 7 "504208-2410" H 2850 1000 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "538-504208-2410" H 2850 900 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=538-504208-2410" H 2850 800 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 2850 700 50  0001 L CNN "RS Part Number"
F 11 "" H 2850 600 50  0001 L CNN "RS Price/Stock"
	1    2000 1400
	1    0    0    -1  
$EndComp
$Comp
L STOD32A:STOD32A U?
U 1 1 5D3AB1D7
P 2850 5400
AR Path="/5D3AB1D7" Ref="U?"  Part="1" 
AR Path="/5D3A7172/5D3AB1D7" Ref="U2"  Part="1" 
F 0 "U2" H 2675 5525 50  0000 C CNN
F 1 "STOD32A" H 2675 5434 50  0000 C CNN
F 2 "QFN_16_NEW:QFN-16-1EP_3x3mm_P0.5mm_EP1.45x1.45mm" H 2850 5400 50  0001 C CNN
F 3 "" H 2850 5400 50  0001 C CNN
	1    2850 5400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D3AB1DD
P 1850 2900
AR Path="/5D3AB1DD" Ref="#PWR?"  Part="1" 
AR Path="/5D3A7172/5D3AB1DD" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 1850 2650 50  0001 C CNN
F 1 "GND" H 1855 2727 50  0000 C CNN
F 2 "" H 1850 2900 50  0001 C CNN
F 3 "" H 1850 2900 50  0001 C CNN
	1    1850 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1400 1850 1400
Wire Wire Line
	1850 1400 1850 1700
Wire Wire Line
	2000 1500 1800 1500
Text GLabel 1800 1500 0    50   Input ~ 0
DSI_D0N
Wire Wire Line
	2000 1600 1800 1600
Text GLabel 1800 1600 0    50   Input ~ 0
DSI_D0P
$Comp
L DFE201610E-4R7M=P2:DFE201610E-4R7M=P2 L?
U 1 1 5D3AB1F1
P 2300 6500
AR Path="/5D3AB1F1" Ref="L?"  Part="1" 
AR Path="/5D3A7172/5D3AB1F1" Ref="L1"  Part="1" 
F 0 "L1" H 2700 6725 50  0000 C CNN
F 1 "DFE201610E-4R7M=P2" H 2700 6634 50  0000 C CNN
F 2 "DFE201610E-4R7M=P2:INDC2016X100N" H 2950 6550 50  0001 L CNN
F 3 "https://psearch.en.murata.com/inductor/product/DFE201610E-4R7M_.pdf" H 2950 6450 50  0001 L CNN
F 4 "Murata DFE201610E Series Type 2016 Shielded Wire-wound SMD Inductor with a Metal Alloy Core, 4.7 uH +/-20%" H 2950 6350 50  0001 L CNN "Description"
F 5 "1" H 2950 6250 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 2950 6150 50  0001 L CNN "Manufacturer_Name"
F 7 "DFE201610E-4R7M=P2" H 2950 6050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-DFE201610E-4R7MP2" H 2950 5950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-DFE201610E-4R7MP2" H 2950 5850 50  0001 L CNN "Mouser Price/Stock"
F 10 "1453500" H 2950 5750 50  0001 L CNN "RS Part Number"
F 11 "https://uk.rs-online.com/web/p/products/1453500" H 2950 5650 50  0001 L CNN "RS Price/Stock"
	1    2300 6500
	1    0    0    -1  
$EndComp
$Comp
L DFE201610E-4R7M=P2:DFE201610E-4R7M=P2 L?
U 1 1 5D3AB1FF
P 2300 6750
AR Path="/5D3AB1FF" Ref="L?"  Part="1" 
AR Path="/5D3A7172/5D3AB1FF" Ref="L2"  Part="1" 
F 0 "L2" H 2700 6975 50  0000 C CNN
F 1 "DFE201610E-4R7M=P2" H 2700 6884 50  0000 C CNN
F 2 "DFE201610E-4R7M=P2:INDC2016X100N" H 2950 6800 50  0001 L CNN
F 3 "https://psearch.en.murata.com/inductor/product/DFE201610E-4R7M_.pdf" H 2950 6700 50  0001 L CNN
F 4 "Murata DFE201610E Series Type 2016 Shielded Wire-wound SMD Inductor with a Metal Alloy Core, 4.7 uH +/-20%" H 2950 6600 50  0001 L CNN "Description"
F 5 "1" H 2950 6500 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 2950 6400 50  0001 L CNN "Manufacturer_Name"
F 7 "DFE201610E-4R7M=P2" H 2950 6300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-DFE201610E-4R7MP2" H 2950 6200 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-DFE201610E-4R7MP2" H 2950 6100 50  0001 L CNN "Mouser Price/Stock"
F 10 "1453500" H 2950 6000 50  0001 L CNN "RS Part Number"
F 11 "https://uk.rs-online.com/web/p/products/1453500" H 2950 5900 50  0001 L CNN "RS Price/Stock"
	1    2300 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1700 1850 1700
Connection ~ 1850 1700
Wire Wire Line
	1850 1700 1850 2600
Wire Wire Line
	2000 1800 1800 1800
Text GLabel 1800 1800 0    50   Input ~ 0
DSI_CLKN
Wire Wire Line
	2000 1900 1800 1900
Text GLabel 1800 1900 0    50   Input ~ 0
DSI_CLKP
Wire Wire Line
	2000 2600 1850 2600
Connection ~ 1850 2600
Wire Wire Line
	1850 2600 1850 2700
Wire Wire Line
	2000 2700 1850 2700
Connection ~ 1850 2700
Wire Wire Line
	1850 2700 1850 2900
Wire Wire Line
	8800 1200 8650 1200
Wire Wire Line
	8650 1200 8650 1600
Wire Wire Line
	9700 1400 9800 1400
Wire Wire Line
	9800 1400 9800 1600
$Comp
L power:GND #PWR?
U 1 1 5D3AB216
P 8650 1850
AR Path="/5D3AB216" Ref="#PWR?"  Part="1" 
AR Path="/5D3A7172/5D3AB216" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 8650 1600 50  0001 C CNN
F 1 "GND" H 8655 1677 50  0000 C CNN
F 2 "" H 8650 1850 50  0001 C CNN
F 3 "" H 8650 1850 50  0001 C CNN
	1    8650 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D3AB21C
P 9800 1850
AR Path="/5D3AB21C" Ref="#PWR?"  Part="1" 
AR Path="/5D3A7172/5D3AB21C" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 9800 1600 50  0001 C CNN
F 1 "GND" H 9805 1677 50  0000 C CNN
F 2 "" H 9800 1850 50  0001 C CNN
F 3 "" H 9800 1850 50  0001 C CNN
	1    9800 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 1500 9900 1500
Wire Wire Line
	9900 1500 9900 1100
Wire Wire Line
	8800 1500 8600 1500
Wire Wire Line
	8600 1500 8600 1100
Text GLabel 8800 1300 0    50   Input ~ 0
DSI_RST
Text GLabel 9700 1200 2    50   Input ~ 0
DSI_INT
Wire Wire Line
	8800 1600 8650 1600
Connection ~ 8650 1600
Wire Wire Line
	8650 1600 8650 1850
Wire Wire Line
	9700 1600 9800 1600
Connection ~ 9800 1600
Wire Wire Line
	9800 1600 9800 1850
$Comp
L power:VDD #PWR?
U 1 1 5D3AB22E
P 8600 1100
AR Path="/5D3AB22E" Ref="#PWR?"  Part="1" 
AR Path="/5D3A7172/5D3AB22E" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 8600 950 50  0001 C CNN
F 1 "VDD" H 8617 1273 50  0000 C CNN
F 2 "" H 8600 1100 50  0001 C CNN
F 3 "" H 8600 1100 50  0001 C CNN
	1    8600 1100
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 5D3AB234
P 9900 1100
AR Path="/5D3AB234" Ref="#PWR?"  Part="1" 
AR Path="/5D3A7172/5D3AB234" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 9900 950 50  0001 C CNN
F 1 "VDD" H 9917 1273 50  0000 C CNN
F 2 "" H 9900 1100 50  0001 C CNN
F 3 "" H 9900 1100 50  0001 C CNN
	1    9900 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 1400 8450 1400
Text GLabel 8400 1400 0    50   Input ~ 0
I2C1_SDA
Wire Wire Line
	9700 1300 10100 1300
Text GLabel 10300 1300 2    50   Input ~ 0
I2C1_SCL
Text Notes 9000 700  0    50   ~ 0
DISPLAY TOUCH\n
$Comp
L 100nF:GRM022R61A104ME01L C12
U 1 1 5D421BF2
P 10250 1100
F 0 "C12" H 10500 1365 50  0000 C CNN
F 1 "GRM022R61A104ME01L" H 10500 1274 50  0000 C CNN
F 2 "100nF:CAPC0402X22N" H 10600 1150 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/GRM022R61A104ME01L.pdf" H 10600 1050 50  0001 L CNN
F 4 "Multilayer Ceramic Capacitors MLCC - SMD/SMT" H 10600 950 50  0001 L CNN "Description"
F 5 "0.22" H 10600 850 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 10600 750 50  0001 L CNN "Manufacturer_Name"
F 7 "GRM022R61A104ME01L" H 10600 650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-GRM022R61A104ME1L" H 10600 550 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-GRM022R61A104ME1L" H 10600 450 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 10600 350 50  0001 L CNN "RS Part Number"
F 11 "" H 10600 250 50  0001 L CNN "RS Price/Stock"
	1    10250 1100
	1    0    0    -1  
$EndComp
$Comp
L 100nF:GRM022R61A104ME01L C11
U 1 1 5D42339F
P 7800 1100
F 0 "C11" H 8050 1365 50  0000 C CNN
F 1 "GRM022R61A104ME01L" H 8050 1274 50  0000 C CNN
F 2 "100nF:CAPC0402X22N" H 8150 1150 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/GRM022R61A104ME01L.pdf" H 8150 1050 50  0001 L CNN
F 4 "Multilayer Ceramic Capacitors MLCC - SMD/SMT" H 8150 950 50  0001 L CNN "Description"
F 5 "0.22" H 8150 850 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 8150 750 50  0001 L CNN "Manufacturer_Name"
F 7 "GRM022R61A104ME01L" H 8150 650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-GRM022R61A104ME1L" H 8150 550 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-GRM022R61A104ME1L" H 8150 450 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 8150 350 50  0001 L CNN "RS Part Number"
F 11 "" H 8150 250 50  0001 L CNN "RS Price/Stock"
	1    7800 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 1100 9900 1100
Connection ~ 9900 1100
Wire Wire Line
	10750 1100 10750 1600
Wire Wire Line
	10750 1600 9800 1600
Wire Wire Line
	8300 1100 8600 1100
Connection ~ 8600 1100
Wire Wire Line
	7800 1100 7800 1600
Wire Wire Line
	7800 1600 8650 1600
$Comp
L Connector:TestPoint TP7
U 1 1 5D3B851E
P 9000 2400
F 0 "TP7" H 9058 2518 50  0000 L CNN
F 1 "TestPoint" H 9058 2427 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 9200 2400 50  0001 C CNN
F 3 "~" H 9200 2400 50  0001 C CNN
	1    9000 2400
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP8
U 1 1 5D3B8E4E
P 9500 2400
F 0 "TP8" H 9558 2518 50  0000 L CNN
F 1 "TestPoint" H 9558 2427 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 9700 2400 50  0001 C CNN
F 3 "~" H 9700 2400 50  0001 C CNN
	1    9500 2400
	1    0    0    -1  
$EndComp
Text GLabel 9000 2400 3    50   Input ~ 0
I2C1_SDA
Text GLabel 9500 2400 3    50   Input ~ 0
I2C1_SCL
Wire Notes Line
	11000 700  7500 700 
Wire Notes Line
	7500 700  7500 3050
Wire Notes Line
	7500 3050 11000 3050
Wire Notes Line
	11000 700  11000 3050
$Comp
L 2k7:ERA2ARB272X R3
U 1 1 5D3EA4A7
P 8450 1400
F 0 "R3" V 8754 1488 50  0000 L CNN
F 1 "ERA2ARB272X" V 8845 1488 50  0000 L CNN
F 2 "2k7 ohm:RESC1005X40N" H 9000 1450 50  0001 L CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDA0000/AOA0000C237.pdf" H 9000 1350 50  0001 L CNN
F 4 "PANASONIC ELECTRONIC COMPONENTS - ERA2ARB272X - RES, THIN FILM, 2K7, 0.1%, 0.063W, 0402" H 9000 1250 50  0001 L CNN "Description"
F 5 "0.4" H 9000 1150 50  0001 L CNN "Height"
F 6 "Panasonic" H 9000 1050 50  0001 L CNN "Manufacturer_Name"
F 7 "ERA2ARB272X" H 9000 950 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 9000 850 50  0001 L CNN "Mouser Part Number"
F 9 "" H 9000 750 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 9000 650 50  0001 L CNN "RS Part Number"
F 11 "" H 9000 550 50  0001 L CNN "RS Price/Stock"
	1    8450 1400
	0    1    1    0   
$EndComp
Connection ~ 8450 1400
Wire Wire Line
	8450 1400 8400 1400
$Comp
L 2k7:ERA2ARB272X R4
U 1 1 5D3EB585
P 10100 1300
F 0 "R4" V 10404 1388 50  0000 L CNN
F 1 "ERA2ARB272X" V 10495 1388 50  0000 L CNN
F 2 "2k7 ohm:RESC1005X40N" H 10650 1350 50  0001 L CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDA0000/AOA0000C237.pdf" H 10650 1250 50  0001 L CNN
F 4 "PANASONIC ELECTRONIC COMPONENTS - ERA2ARB272X - RES, THIN FILM, 2K7, 0.1%, 0.063W, 0402" H 10650 1150 50  0001 L CNN "Description"
F 5 "0.4" H 10650 1050 50  0001 L CNN "Height"
F 6 "Panasonic" H 10650 950 50  0001 L CNN "Manufacturer_Name"
F 7 "ERA2ARB272X" H 10650 850 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 10650 750 50  0001 L CNN "Mouser Part Number"
F 9 "" H 10650 650 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 10650 550 50  0001 L CNN "RS Part Number"
F 11 "" H 10650 450 50  0001 L CNN "RS Price/Stock"
	1    10100 1300
	0    1    1    0   
$EndComp
Connection ~ 10100 1300
Wire Wire Line
	10100 1300 10300 1300
$Comp
L power:VDD #PWR0151
U 1 1 5D3ECE93
P 10100 2000
F 0 "#PWR0151" H 10100 1850 50  0001 C CNN
F 1 "VDD" H 10118 2173 50  0000 C CNN
F 2 "" H 10100 2000 50  0001 C CNN
F 3 "" H 10100 2000 50  0001 C CNN
	1    10100 2000
	-1   0    0    1   
$EndComp
$Comp
L power:VDD #PWR0152
U 1 1 5D3ED8D4
P 8450 2100
F 0 "#PWR0152" H 8450 1950 50  0001 C CNN
F 1 "VDD" H 8468 2273 50  0000 C CNN
F 2 "" H 8450 2100 50  0001 C CNN
F 3 "" H 8450 2100 50  0001 C CNN
	1    8450 2100
	-1   0    0    1   
$EndComp
$EndSCHEMATC
