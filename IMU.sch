EESchema Schematic File Version 4
LIBS:SmartWatch Final-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 5D3B26B2
P 5450 4150
AR Path="/5D3B26B2" Ref="#PWR?"  Part="1" 
AR Path="/5D3B16F5/5D3B26B2" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 5450 3900 50  0001 C CNN
F 1 "GND" H 5455 3977 50  0000 C CNN
F 2 "" H 5450 4150 50  0001 C CNN
F 3 "" H 5450 4150 50  0001 C CNN
	1    5450 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D3B26B8
P 5550 4350
AR Path="/5D3B26B8" Ref="#PWR?"  Part="1" 
AR Path="/5D3B16F5/5D3B26B8" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 5550 4100 50  0001 C CNN
F 1 "GND" H 5555 4177 50  0000 C CNN
F 2 "" H 5550 4350 50  0001 C CNN
F 3 "" H 5550 4350 50  0001 C CNN
	1    5550 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 4150 5550 4350
Wire Wire Line
	5450 2950 5450 2800
$Comp
L power:VDD #PWR0142
U 1 1 5D3E0467
P 5450 2800
F 0 "#PWR0142" H 5450 2650 50  0001 C CNN
F 1 "VDD" H 5467 2973 50  0000 C CNN
F 2 "" H 5450 2800 50  0001 C CNN
F 3 "" H 5450 2800 50  0001 C CNN
	1    5450 2800
	1    0    0    -1  
$EndComp
Text GLabel 6050 3250 2    50   Input ~ 0
IMU_INT1
Text GLabel 6050 3350 2    50   Input ~ 0
IMU_INT2
Connection ~ 5450 2950
Wire Wire Line
	5550 2950 5450 2950
Text GLabel 4500 3850 0    50   Input ~ 0
IMU_CS
Text GLabel 4500 3650 0    50   Input ~ 0
I2C2_SDA
Text GLabel 4500 3750 0    50   Input ~ 0
I2C2_SCL
$Comp
L Sensor_Motion:LSM6DS3 U?
U 1 1 5D3B26AC
P 5450 3550
AR Path="/5D3B26AC" Ref="U?"  Part="1" 
AR Path="/5D3B16F5/5D3B26AC" Ref="U3"  Part="1" 
F 0 "U3" H 6094 3596 50  0000 L CNN
F 1 "LSM6DS3" H 6094 3505 50  0000 L CNN
F 2 "Package_LGA:LGA-14_3x2.5mm_P0.5mm_LayoutBorder3x4y" H 5050 2850 50  0001 L CNN
F 3 "www.st.com/resource/en/datasheet/lsm6ds3.pdf" H 5550 2900 50  0001 C CNN
	1    5450 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 3650 4550 3650
Wire Wire Line
	4850 3750 4700 3750
Wire Wire Line
	4850 3850 4500 3850
$Comp
L 2k7:ERA2ARB272X R5
U 1 1 5D3F0868
P 4550 2950
F 0 "R5" V 4854 3038 50  0000 L CNN
F 1 "ERA2ARB272X" V 4945 3038 50  0000 L CNN
F 2 "2k7 ohm:RESC1005X40N" H 5100 3000 50  0001 L CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDA0000/AOA0000C237.pdf" H 5100 2900 50  0001 L CNN
F 4 "PANASONIC ELECTRONIC COMPONENTS - ERA2ARB272X - RES, THIN FILM, 2K7, 0.1%, 0.063W, 0402" H 5100 2800 50  0001 L CNN "Description"
F 5 "0.4" H 5100 2700 50  0001 L CNN "Height"
F 6 "Panasonic" H 5100 2600 50  0001 L CNN "Manufacturer_Name"
F 7 "ERA2ARB272X" H 5100 2500 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 5100 2400 50  0001 L CNN "Mouser Part Number"
F 9 "" H 5100 2300 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 5100 2200 50  0001 L CNN "RS Part Number"
F 11 "" H 5100 2100 50  0001 L CNN "RS Price/Stock"
	1    4550 2950
	0    1    1    0   
$EndComp
Connection ~ 4550 3650
Wire Wire Line
	4550 3650 4500 3650
$Comp
L 2k7:ERA2ARB272X R6
U 1 1 5D3F0FF9
P 4700 3050
F 0 "R6" V 5004 3138 50  0000 L CNN
F 1 "ERA2ARB272X" V 5095 3138 50  0000 L CNN
F 2 "2k7 ohm:RESC1005X40N" H 5250 3100 50  0001 L CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDA0000/AOA0000C237.pdf" H 5250 3000 50  0001 L CNN
F 4 "PANASONIC ELECTRONIC COMPONENTS - ERA2ARB272X - RES, THIN FILM, 2K7, 0.1%, 0.063W, 0402" H 5250 2900 50  0001 L CNN "Description"
F 5 "0.4" H 5250 2800 50  0001 L CNN "Height"
F 6 "Panasonic" H 5250 2700 50  0001 L CNN "Manufacturer_Name"
F 7 "ERA2ARB272X" H 5250 2600 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 5250 2500 50  0001 L CNN "Mouser Part Number"
F 9 "" H 5250 2400 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 5250 2300 50  0001 L CNN "RS Part Number"
F 11 "" H 5250 2200 50  0001 L CNN "RS Price/Stock"
	1    4700 3050
	0    1    1    0   
$EndComp
Connection ~ 4700 3750
Wire Wire Line
	4700 3750 4500 3750
$Comp
L power:VDD #PWR0153
U 1 1 5D3F1878
P 4550 2950
F 0 "#PWR0153" H 4550 2800 50  0001 C CNN
F 1 "VDD" H 4567 3123 50  0000 C CNN
F 2 "" H 4550 2950 50  0001 C CNN
F 3 "" H 4550 2950 50  0001 C CNN
	1    4550 2950
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR0154
U 1 1 5D3F1E37
P 4700 3050
F 0 "#PWR0154" H 4700 2900 50  0001 C CNN
F 1 "VDD" H 4717 3223 50  0000 C CNN
F 2 "" H 4700 3050 50  0001 C CNN
F 3 "" H 4700 3050 50  0001 C CNN
	1    4700 3050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
